//
//  Helper.swift
//  CRUD
//
//  Created by Calvin Saragih on 12/07/20.
//  Copyright © 2020 Calvin. All rights reserved.
//

import Foundation
import UIKit

class Helper {
    
    static func getValueJson(json:Data,key:String) -> String{
        do {
            if let json = try JSONSerialization.jsonObject(with: json, options: []) as? [String: Any] {
                if let string = json[key] as? String {
                    return string
                }
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
            return ""
        }
        return ""
    }
    
}

extension UIImage{
    
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / self.size.width
        let heightRatio = targetSize.height / self.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension UIViewController{
    
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAddContactDialog(
        inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
        cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
        actionHandler: ((_ firstName: String?,_ secondName: String?,_ age:String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: "Add New Contact", message: "", preferredStyle: .alert)
        let save = UIAlertAction(title: "Save", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            let textField2 = alert.textFields![1] as UITextField
            let textField3 = alert.textFields![2] as UITextField
            if textField.text != "" && textField2.text != "" && textField3.text != ""  && textField3.text?.isInt ?? false {
                actionHandler?(textField.text,textField2.text,textField3.text)
            } else {
                
            }
        }
        
        
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter Contact First Name"
            textField.textColor = .black
        }
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter Contact Last Name"
            textField.textColor = .black
        }
        
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter Contact Age"
            textField.textColor = .black
        }
        
        alert.addAction(save)
        
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in }
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension String {
    var isInt: Bool {
        return Int(self) != nil
    }
}

extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
