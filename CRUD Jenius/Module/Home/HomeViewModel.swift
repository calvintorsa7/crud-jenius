//
//  HomeViewModel.swift
//  CRUD
//
//  Created by Calvin Saragih on 12/07/20.
//  Copyright © 2020 Calvin. All rights reserved.
//

import Foundation

class HomeViewModel {
    
    public enum ErrorReporter {
        case internetError(String)
        case serverMessage(String)
    }
    
    public var contactData:[Contact] = [Contact](){
        didSet{
            self.reloadContactData?()
        }
    }
    public var error : ((ErrorReporter)->Void)?
    public var reloadContactData:(()->())?
    
    public func addContact(firstName:String,lastName:String,age:Int)  {
        let param = ["firstName":firstName,"lastName":lastName,"age":age,"photo":"www.google.com"] as [String : Any]
        ApiHelper.requestDataParam(url: "", method: .post, parameters: param, completion:{
            (result) in
            switch result {
            case .success(let returnJson) :
                self.getAllContact()
               print(returnJson)
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.error?(.internetError("Check your Internet connection."))
                case .authorizationError(let errorJson):
                    let errorMessage = Helper.getValueJson(json: errorJson, key: "message")
                    self.error?(.internetError(errorMessage))
                default:
                    self.error?(.serverMessage("Unknown Error"))
                }
                
            }
        })
    }
    
    public func getAllContact()  {
        ApiHelper.requestData(url: "", method: .get, parameters: nil, completion:{
            (result) in
            let decoder = JSONDecoder()
            switch result {
            case .success(let returnJson) :
                do {
                    let response = try decoder.decode(Response<Contact>.self, from: returnJson)
                    self.contactData = response.data
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.error?(.internetError("Check your Internet connection."))
                case .authorizationError(let errorJson):
                    let errorMessage = Helper.getValueJson(json: errorJson, key: "message")
                    self.error?(.internetError(errorMessage))
                default:
                    self.error?(.serverMessage("Unknown Error"))
                }
                
            }
        })
    }
    
    public func updateContact()  {
        ApiHelper.requestData(url: "/photos", method: .get, parameters: nil, completion:{
            (result) in
            let decoder = JSONDecoder()
            switch result {
            case .success(let returnJson) :
                do {
                    let photos = try decoder.decode([Contact].self, from: returnJson)
                    print(photos)
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.error?(.internetError("Check your Internet connection."))
                case .authorizationError(let errorJson):
                    let errorMessage = Helper.getValueJson(json: errorJson, key: "message")
                    self.error?(.internetError(errorMessage))
                default:
                    self.error?(.serverMessage("Unknown Error"))
                }
                
            }
        })
    }
    
    
    public func deleteContact(id:String)  {
        print(id)
        ApiHelper.requestData(url: "/\(id)", method: .delete, parameters: nil, completion:{
            (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                self.getAllContact()
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.error?(.internetError("Check your Internet connection."))
                case .authorizationError(let errorJson):
                    let errorMessage = Helper.getValueJson(json: errorJson, key: "message")
                    self.error?(.internetError(errorMessage))
                default:
                    self.error?(.serverMessage("Unknown Error"))
                }
                
            }
        })
    }
    
}
