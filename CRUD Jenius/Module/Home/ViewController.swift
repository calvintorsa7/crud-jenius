//
//  ViewController.swift
//  CRUD Jenius
//
//  Created by Calvin Saragih on 12/07/20.
//  Copyright © 2020 Calvin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var viewModel = HomeViewModel()
    @IBOutlet weak var contactCollectionView: UICollectionView!
    
    private let cellIdentifier = "ContactViewCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "CRUD Jenius"
        setCollectionView()
        bindingData()
        
    }

    @IBAction func addContactOnClicked(_ sender: Any) {
        self.showAddContactDialog(actionHandler: { firstName, lastName, age in
            self.viewModel.addContact(firstName: firstName! , lastName: lastName! , age: Int(age!)! )
        })
    }
    
    private func setCollectionView(){
        self.contactCollectionView.delegate = self
        self.contactCollectionView.dataSource = self
    
        self.viewModel.getAllContact()
    }
    
    private func bindingData(){
        self.viewModel.reloadContactData = { [weak self] () in
            DispatchQueue.main.async {
                self?.contactCollectionView.reloadData()
            }
        }
        
       self.viewModel.error = { (error) in
            switch error {
            case .internetError(let message):
                DispatchQueue.main.async {self.showToast(message: message, font: .systemFont(ofSize: 12.0))
                }
            case .serverMessage(let message):
               DispatchQueue.main.async { self.showToast(message: message, font: .systemFont(ofSize: 12.0))
                }
            }
        }
    }
    
}

extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.contactData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ContactViewCell
        let firstName = self.viewModel.contactData[indexPath.item].firstName
        let lastName = self.viewModel.contactData[indexPath.item].lastName
        cell.nameLabel.text = "\(firstName) \(lastName)"
        cell.ageLabel.text = String(self.viewModel.contactData[indexPath.item].age)
        cell.layoutIfNeeded()
        cell.btnTapAction = {
            () in self.viewModel.deleteContact(id: self.viewModel.contactData[indexPath.item].id)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 140)
    }
    
  
    
}
