//
//  ContactViewCell.swift
//  CRUD Jenius
//
//  Created by Calvin Saragih on 12/07/20.
//  Copyright © 2020 Calvin. All rights reserved.
//

import UIKit

class ContactViewCell: UICollectionViewCell {

    var btnTapAction : (()->())?
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

       
    }
    
    @IBAction func deleteOnClicked(_ sender: Any) {
        btnTapAction?()
    }
    

}
