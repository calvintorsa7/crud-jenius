//
//  Response.swift
//  CRUD
//
//  Created by Calvin Saragih on 12/07/20.
//  Copyright © 2020 Calvin. All rights reserved.
//

import Foundation

class Response<T>:Codable where T:Codable {
    
    
    let message: String
    let data: [T]
    
    private enum CodingKeys: String, CodingKey {
        case message = "message"
        case data = "data"
    }
    
    
    
}
