//
//  Contact.swift
//  CRUD
//
//  Created by Calvin Saragih on 12/07/20.
//  Copyright © 2020 Calvin. All rights reserved.
//

import Foundation

struct Contact: Codable {

    let id: String
    let firstName: String
    let lastName: String
    let age: Int
    let photo: String

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case firstName = "firstName"
        case lastName = "lastName"
        case age = "age"
        case photo = "photo"
    }



}
